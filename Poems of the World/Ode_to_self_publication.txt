Ode to Self-Publication


I wish I were well-versed in verse
But this is my first creation		
Yet for better or for worse
I pursue its publication

Before I try my mind is freed
It rhymes the need with revelation
Denies to heed the iamb creed
Unleashing a rhythm of dactyle persuasion

Well-knowing that no-one would publish this sonnet
Which is also an ode to the DIY tenet
I elect to submit to my own application
Entertained by the notion of a free internet
Where creative and technical spirits are wed
I bet this will make for a great illustration