const workApi = require("./workApi")
const collectionSchema = require("./schemas/collectionSchema")
const hyperHelpers = require("./hyperHelpers")

class CollectionContainsUnwritableWorksError extends Error {
    constructor(key, collectionJson, message = "", ...args) {
      super(message, ...args);
      this.key = key
      this.collectionJson = collectionJson
      this.message = message + ` Invalid CollectionOfOwnWorks: the hyperdrive of at least one of the works (${key}) is not writable by the given hyperspace client` +
                        ` (collection: ${JSON.stringify(collectionJson)})`
        
    }
  }

async function validateCollectionOfOwnWork(collection, client){
    //console.log("coll: ", collection)
    collectionSchema.validateCollection(collection)
    //console.log("Validated collection", collection)
    for(const work of collection.works){
        //console.log("Validating work", work)
        await hyperHelpers.read_work_as_json(work, client) // Validates the work
        //console.log("Validated")
        const drive = await hyperHelpers.getHyperdriveFromClient(client, work)
        //console.log("writable: ", drive.writable)
        if(!drive.writable){
            throw new CollectionContainsUnwritableWorksError(work, collection)
        }
    }
    return true    
}


async function createCollectionOfOwnWork(title, workHashes, client){
    collection = collectionSchema.createCollectionFromTitleAndWorkHashes(title, workHashes)
    await validateCollectionOfOwnWork(collection, client)
    return collection
}


module.exports.validateCollectionOfOwnWork = validateCollectionOfOwnWork
module.exports.createCollectionOfOwnWork = createCollectionOfOwnWork
