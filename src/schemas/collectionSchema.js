const Ajv2020 = require("ajv/dist/2020")
const ajv = new Ajv2020()
const collectionSchema = require('../static/collectionSchema.json'); //Assumes schema never changes during execution
const validateCollectionSchema= ajv.compile(collectionSchema)
schemas = require("./schemas")


function validateCollection(collectionAsJson){
    //console.log(collectionAsJson)
    return schemas.validateSchema(collectionAsJson, validateCollectionSchema)
}


class Collection{
    constructor(obj){
        Object.assign(this, obj)
        validateCollection(this)
      }

    static get TITLE_KEY(){
        return "title"
    }

    static get WORKS_KEY(){
        return "works"
    }

    static get COLLECTION_KEY(){
        return "collection"
    }

    get works(){
        return this[Collection.COLLECTION_KEY][Collection.WORKS_KEY]
    }
}


function createCollectionFromTitleAndWorkHashes(title, workHashes){    
    const collectionAsJson = {
        [Collection.TITLE_KEY]: title,
        [Collection.COLLECTION_KEY]: {[Collection.WORKS_KEY]: workHashes}
    }
    return new Collection(collectionAsJson)
}

module.exports.Collection = Collection
module.exports.validateCollection = validateCollection
module.exports.createCollectionFromTitleAndWorkHashes = createCollectionFromTitleAndWorkHashes

