

function validateSchema(jsonContent, validationFunction, throwErrorIfInvalid=true){
    const isValid = validationFunction(jsonContent)  
    if (!isValid){
        if(throwErrorIfInvalid){
            info =  `Validation error(s): ${JSON.stringify(validationFunction.errors)} \n(json: ${JSON.stringify(jsonContent)} )`
            throw new Error(info)    
        }
        else{
            //ToDo Think on whether to print warning or not
            //console.warn(info)
            return false
        }
    } 
    return true
}


module.exports.validateSchema = validateSchema
