const Ajv2020 = require("ajv/dist/2020")
const ajv = new Ajv2020()
const workSchema = require('../static/workSchema.json'); //Assumes schema never changes during execution
const validateWorkSchema = ajv.compile(workSchema)
const {validateSchema} = require("./schemas")

const HASH_KEY = "hash"
const TITLE_KEY = "title"

function validateWork(workAsJson){
    return validateSchema(workAsJson, validateWorkSchema)
}

function getHypercoreHashRegex(){
    return workSchema["$defs"]["hypercoreHash"]["pattern"]
}

class Work{
    constructor(obj){
        Object.assign(this, obj)
        validateWork(this)
      }

    static get TITLE_KEY(){
        return "title"
    }

    static get HASH_KEY(){
        return "hash"
    }
}


module.exports.validateWork = validateWork
module.exports.getHypercoreHashRegex = getHypercoreHashRegex
module.exports.HASH_KEY = HASH_KEY
module.exports.TITLE_KEY = TITLE_KEY
module.exports.Work = Work

