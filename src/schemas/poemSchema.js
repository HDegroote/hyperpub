const Ajv2020 = require("ajv/dist/2020")
const ajv = new Ajv2020()
const poemSchema = require("../static/poemSchema.json")
const validatePoemSchema = ajv.compile(poemSchema)
const lineSchema = poemSchema["$defs"]["lineSchema"]
const validateLineSchema = ajv.compile(lineSchema)
verseSchema = Object.assign({}, poemSchema["$defs"]["verseSchema"]);
verseSchema["$defs"] = poemSchema["$defs"] //ToDo don't use this ad hoc solution to include the reference to lineSchema
validateVerseSchema = ajv.compile(verseSchema)
chapterSchema = Object.assign({}, poemSchema["$defs"]["chapterSchema"]);
chapterSchema["$defs"] = poemSchema["$defs"] //ToDo don't use this ad hoc solution to include the reference to lineSchema and verseSchema
validateChapterSchema = ajv.compile(chapterSchema)

const {validateSchema} = require("./schemas")


function validatePoem(poemAsJson){
    return validateSchema(poemAsJson, validatePoemSchema)
}


function validateLine(lineAsJson){
    return validateSchema(lineAsJson, validateLineSchema)
}


function validateVerse(verseAsJson){
    return validateSchema(verseAsJson, validateVerseSchema)
}


function validateChapter(chapterAsJson){
    return validateSchema(chapterAsJson, validateChapterSchema)
}


class Line{    
    constructor(obj){
        Object.assign(this, obj)
        validateLine(this)
      }

    static get LINE_KEY(){
        return "line"
    }

    static get TEXT_KEY(){
        return "text"
    }

    static get POEM_KEY(){
        return "poem"        
    }

    static get INDENT_KEY(){
        return "indent"
    }
    
    get text(){
        return this[Line.LINE_KEY][Line.TEXT_KEY]
    }

    get indent(){
        let indentLvl = this[Line.LINE_KEY][Line.INDENT_KEY]
        if(indentLvl === undefined){
            indentLvl = 0
        }
        return indentLvl
        
    }
}


class Verse{    
    constructor(obj){
        Object.assign(this, obj)
        validateVerse(this)
      }

    static get VERSE_KEY(){
        return "verse"
    }

    get lines(){
        return this[Verse.VERSE_KEY]
    }
}


class Chapter{
    constructor(obj){
        Object.assign(this, obj)
        validateChapter(this)
    }

    static get CHAPTER_KEY(){
        return "chapter"
    }

    static get TITLE_KEY(){
        return "title"
    }

    static get CONTENT_KEY(){
        return "content"
    }

    get content(){
        return this.chapter.content
    }

}


class Poem{
    constructor(obj){
        Object.assign(this, obj)
        validatePoem(this)
      }

    static get TITLE_KEY(){
        return "title"
    }

    static get CONTENT_KEY(){
        return "content"
    }

    static get POEM_KEY(){
        return "poem"
    }

    static get EPIGRAPH_KEY(){
        return "epigraph"
    }

    get content(){
        return this.poem.content
    }

    get verses(){
        return this.content
    }

    get epigraph(){        
        if(this.poem.epigraph){
            return this.poem.epigraph
        }
        else{
            return null
        }        
    }
}


function createVerseFromLines(lines){
    const verseJson = {[Verse.VERSE_KEY]: verseLines}
    return new Verse(verseJson)
}


function createLineFromStr(text, indent=0){
    /* Note: only explicitly adds the indent if it is strictly greater than 0*/
    const lineAsJson = {[Line.LINE_KEY]: {[Line.TEXT_KEY]: text}}
    if(indent > 0){
        lineAsJson[Line.LINE_KEY][Line.INDENT_KEY] = indent
    }
    return new Line(lineAsJson)
}


function createChapterFromContentAndTitle(verses, title=""){
    const chapterJson = {
        [Chapter.CHAPTER_KEY]: {
            [Chapter.CONTENT_KEY]: verses},
            [Chapter.TITLE_KEY]: title
        }
    return new Chapter(chapterJson)
}


function createPoemFromTitleAndContent(title, content, epigraph=null){
    let poemAsJson = {
        [Poem.TITLE_KEY]: title,
        [Poem.POEM_KEY]: {[Poem.CONTENT_KEY]: content}
    }
    if(epigraph!==null){
        poemAsJson = addOrReplaceEpigraphToPoem(poemAsJson, epigraph)
    }
    return new Poem(poemAsJson)
}


function addOrReplaceEpigraphToPoem(jsonPoem, epigraph){
    const updatedPoem = Object.assign({}, jsonPoem);
    updatedPoem[Poem.POEM_KEY][Poem.EPIGRAPH_KEY] = epigraph
    return new Poem(updatedPoem)
}

module.exports.validatePoem = validatePoem
module.exports.validateLine = validateLine
module.exports.validateVerse = validateVerse
module.exports.Line = Line
module.exports.createLineFromStr = createLineFromStr
module.exports.Verse = Verse
module.exports.createVerseFromLines = createVerseFromLines
module.exports.Chapter = Chapter
module.exports.validateChapter = validateChapter
module.exports.createChapterFromVersesAndTitle = createChapterFromContentAndTitle
module.exports.Poem = Poem
module.exports.createPoemFromTitleAndContent = createPoemFromTitleAndContent
module.exports.addOrReplaceEpigraphToPoem = addOrReplaceEpigraphToPoem