const workApi = require("./workApi")
const { promises: fs } = require("fs");
const collectionSchema = require("./schemas/collectionSchema")
const Collection = collectionSchema.Collection
const {validateCollectionOfOwnWork} = require("./ownCollectionHelpers")


const ownWorksJsonLoc = "./ownWorks.json"


async function readOwnCollectionFromFS(location=ownWorksJsonLoc){
    let ownCollectionContent
    try {        
        ownCollectionContent = await fs.readFile(location, 'utf8')
    } catch (err) {
        const msg = `Warning: could not load the collection of own works from the expected location ${location}. ` +
        "Will create a new folder to store it (original error: '" + err.toString() + "'"
        console.warn(msg)
        ownCollectionContent = JSON.stringify(collectionSchema.createCollectionFromTitleAndWorkHashes("All works created by the owner of this hyperspace instance", []))
    }
    return ownCollectionContent

}

async function getOwnCollection(client, location=ownWorksJsonLoc){
    const ownCollectionContent = await readOwnCollectionFromFS(location)
    const ownWorksCollectionJson = JSON.parse(ownCollectionContent.toString())
    const ownWorksCollection = new Collection(ownWorksCollectionJson)
    await validateCollectionOfOwnWork(ownWorksCollection, client)
    return ownWorksCollection;
}


async function saveOwnCollection(collectionJson, client, location=ownWorksJsonLoc){
    await validateCollectionOfOwnWork(collectionJson, client)
    await fs.writeFile(location, JSON.stringify(collectionJson), "utf-8")    
}


async function addHashesToOwnCollection(newHashes, client){
    const ownCollection = await getOwnCollection(client)
    ownCollection.works.push(...newHashes)
    await saveOwnCollection(ownCollection, client)
}


async function addWorksToOwnCollection(workJsons, client){
    /**Adds the given works (in json format) to the own collection, if the result is a valid ownCollection
     * (if not, the ownCollection does not change)
    */
    const ownCollection = await getOwnCollection(client)
    const allAddedHashes = []
    for(const jsonWork of workJsons){
        const workHash = await workApi.create_work(jsonWork, client)
        ownCollection.works.push(workHash)
        allAddedHashes.push(workHash)
    }
    await saveOwnCollection(ownCollection, client)
    return allAddedHashes
}


module.exports.addWorksToOwnCollection = addWorksToOwnCollection
module.exports.getOwnCollection = getOwnCollection
module.exports.addHashesToOwnCollection = addHashesToOwnCollection