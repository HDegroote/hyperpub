const { createCollectionFromTitleAndWorkHashes } = require("./schemas/collectionSchema")
const poemSchema = require("./schemas/poemSchema")

const newline_regex = (/\r\n|\r|\n/g) // Ideally would use something like python's splitlines https://docs.python.org/3/library/stdtypes.html#str.splitlines



function cleanNewlinesAndTrimWhiteSpaceFromBlankLines(text){
    lines = text.split(newline_regex) //DEVNOTE: full split not required if ever efficiency becomes an issue (unlikely)
    newLines = []
    for(line of lines){
        const trimmedLine = line.trim()
        /* DEVNOTE: somewhat ad hoc construction to only tirm whitespace of lines which are in effect blank lines
            This is done because tabs hold semantic information as to the amount of indent of a line, which is parsed and cleaned later
        */
        if(trimmedLine === ""){
            newLines.push(trimmedLine)
        }
        else{
            newLines.push(line)
        }
    }
    return newLines.join("\n")
}


function splitInTitleAndBody(txt){
    /*
        Throws an error when no blank line is found after the perceived title, to avoid unexpected behaviour
        Note: Assumes the text is clean (as after running cleanNewlinesAndTrimWhiteSpace)
    */    
    lines = txt.split("\n") //DEVNOTE: full split not required if ever efficiency becomes an issue (unlikely)    
    title = lines[0].trim()
    if(lines[1].trim() !== ""){
        throw Error(`Expected a blank line after the title, but found '${lines[1]}'' (extracted title: '${title}')`)
    }
    body = lines.slice(2).join("\n")    
    return [title, body]
}


function getNrOfTabsBeforeIndex(line, index){
    let nrTabs = 0
    tabIndex = line.indexOf("\t")
    while(tabIndex >= 0 && tabIndex < index){
        nrTabs += 1
        tabIndex = line.indexOf("\t", tabIndex +1)
    }
    return nrTabs
}


function parseLine(line){
    const trimmedLine = line.trim()
    const firstCharIndex = line.indexOf(trimmedLine[0])
    const indent = getNrOfTabsBeforeIndex(line, firstCharIndex)
    const lineAsJson = poemSchema.createLineFromStr(line.trim(), indent)
    return lineAsJson
}

function parseVerse(txtVerse){
    // Assumes eachn line of txtVerse contains at least 1 non-whitespace character
    verseLines = []
    lines = txtVerse.split("\n")
    for(line of lines){
        const lineAsJson = parseLine(line)
        verseLines.push(lineAsJson)
    }
    const verseAsJson = poemSchema.createVerseFromLines(verseLines)
    return verseAsJson
}


function parseChapterContent(txtChapter){
    const txtVerses = body.split("\n\n")
    const content = []    
    for(const txtVerse of txtVerses){
        const verseAsJson = parseVerse(txtVerse)
        content.push(verseAsJson)
    }    
    return content
}


function parseTxtPoem(poem){
    const spaceCleanedPoem = cleanNewlinesAndTrimWhiteSpaceFromBlankLines(poem)
    const [title, body] = splitInTitleAndBody(spaceCleanedPoem)
    const txtChapters = body.split("\n".repeat(3))
    const content = []
    //ToDo reconsider ad-hoc if-else logic
    if(txtChapters.length == 1){        
        content.push(...parseChapterContent(txtChapters[0]))
    }
    else{
        for(const txtChapter of txtChapters){
            const [chaptertitle, chapterBody] = splitInTitleAndBody(txtChapter)
            const chapterContent = parseChapterContent(chapterBody)
            const jsonChapter = poemSchema.createChapterFromVersesAndTitle(chapterContent, chaptertitle)
            content.push(jsonChapter)
        }    
    }
    const poemAsJson = poemSchema.createPoemFromTitleAndContent(title, content)
    return poemAsJson    
}


function parseTxtCollection(collectionTxt){
    const [title, body] = splitInTitleAndBody(collectionTxt)
    const workHashesDirty = body.trim().split("\n")
    const workHashes = []
    for(workHash of workHashesDirty){
        workHashes.push(workHash.trim())
    }
    const collection = createCollectionFromTitleAndWorkHashes(title, workHashes)
    return collection    
}



module.exports.parseTxtPoem = parseTxtPoem
module.exports.parseTxtCollection = parseTxtCollection
module.exports.getNrOfTabsBeforeIndex = getNrOfTabsBeforeIndex
module.exports.parseLine = parseLine
module.exports.splitInTitleAndBody = splitInTitleAndBody
