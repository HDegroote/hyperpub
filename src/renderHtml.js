const hyperHelpers = require("./hyperHelpers")
const {Work} = require("./schemas/workSchema")
const {Collection, validateCollection} = require("./schemas/collectionSchema")
const {Poem, Verse,Chapter, validatePoem, validateChapter, validateVerse, validateLine, Line} = require("./schemas/poemSchema")
const { promises: fs } = require("fs");
const schemas = require("./schemas/schemas")
const json2html = require('node-json2html');
const beautify = require("js-beautify").html
const path = require('path');

const STANDARD_COLLECTION_STYLESHEET = "collection_style.css"
const STANDARD_POEM_STYLESHEET = "poem_style.css"

//ToDo Find cleaner way to do paths relative to this file, or other solution in general
const POEM_STYLESHEET_LOC = path.resolve(path.join(__dirname, "./static/poem_style.css"))
const COLLECTION_STYLESHEET_LOC = path.resolve(path.join(__dirname, "./static/collection_style.css"))


class NoValidatingFunctionFoundError extends Error {
    constructor(jsonObject, schemaMapping, message = "", ...args) {
      super(message, ...args);
      this.jsonObject = jsonObject
      this.schemaMapping = schemaMapping      
      this.message = message + `No function which validates found for object ${this.jsonObject} (Validation functions tried: ${this.schemaMapping})`        
    }
  }




function getHtmlHeader(stylesheet){
    const res = {"<>":"HEAD", "html":[
        {"<>":"title","text":"${title}"},
        {"<>":"link", "rel": "stylesheet", "href":`${stylesheet}`}, //ToDo check if this is a security vulnerability
        {"<>":"meta", "charset":"utf-8"}
    ]}
    return res
}

function getCollectionHtmlTemplate(stylesheet=STANDARD_COLLECTION_STYLESHEET){
    //ToDo create only once
    const template = {"<>":"html","html": [	
        getHtmlHeader(stylesheet),
        {"<>":"body","html": [
            {"<>":"div","class":"collection", "html": [
                {"<>":"HEADER","html": [
                    {"<>":"H1","text":"${title}"}
                ]},   
                
                {'<>':'ul', "html": [
                    {'<>':'li', "class":"collectionPage", 'obj':function(){return(this.works)},'html':[		
                        {"<>":"H3","html": [
                            {"<>":"a","href": "hyper://${hash}", "text":"${title}"}
                        ]}
                    ]}


                ]}
            ]}
        ]}

    ]};
    return template
}


async function createHtmlableCollection(collection, client){
    const hashTag = Work.HASH_KEY
    const titleTag = "title"
    const expandedCollection = Object.assign({}, collection);
    expandedCollection.works = []
    for(const workHash of new Collection(collection).works){
        const work = await hyperHelpers.read_work_as_json(workHash, client)
        const workInfo = {[titleTag]: work.title, [hashTag]: work.hash}
        expandedCollection.works.push(workInfo)
    }
    return expandedCollection
} 

async function renderCollectionHtml(collection, client, stylesheet=STANDARD_COLLECTION_STYLESHEET){
    const expandedCollection = await createHtmlableCollection(collection, client)
    //console.log("expanded ", JSON.stringify(expandedCollection))
    const template = getCollectionHtmlTemplate(stylesheet)
    
    const uglyRes = json2html.render(expandedCollection,template)
    const res = beautify(uglyRes)
    return res
}


async function renderPoemHtml(poem, client, stylesheet=STANDARD_POEM_STYLESHEET){
    /* DEVNOTE: This method has support for dynamically identifying which component to apply, based on the getJson2htmlComponentName function.
        This was done primarily to support recursive schemas, for example when a poem can consist of both chapters or verses (or even a mixture)
        Do note that the available components and their mapping from validation functions must be explicitly defined
        

    */
    poem = new Poem(poem) //Ensure proper encapsulation with acessor methods + schema check

    lineRenderJson = {"<>": "span", "class":"poem_line", "html": [
        {"text": function(obj, index){
            emSpace = "\u2003"
            const line = new Line(obj)
            const res = emSpace.repeat(2*line.indent) + line.text
            return res}
        }
    ] }

    //DEVNOTE: if ever verses can contain more than just lines, pass through getJson2htmlComponentName
    verseRenderJson = {'<>':'span', "class":"verse", 'html':[                        
        {'obj':function(){return(new Verse(this).lines)}, "html": [
            {"[]": "line"},
            {"html": "<BR/>"} /*:[		
            //{"<>":"span", "class": "poem_line", "text": "${this}"}
            //{"<>":"a","href": "${hash}",4 "text":"${title}"}*/
        ]}
    ]}

    chapterRenderJson = {'<>':'span', "class":"chapter", 'html':[  
            {'<>': "H3", "class": "poem_chapter_title", "text": "${title}"},        
            {'<>':'span', "class":"verse", 'obj':function(){return(new Chapter(this).content)},'html':[     
                // DEVNOTE: if ever a chapter can contain more than verses, make this dynamic with getJson2htmlComponentName
                {"[]": "verse"}
            ]},
            {"html": "<BR/>"},
            {"<>": "H3", "class": "flourish", "text": "***"}, //ToDo Find more elegant way to do flourishes (and have nicer ones)                            
            {"html": "<BR/>"}                  
        ]}
    
        /*
          <div class="quote_env">
            <div class="quote">
        */
    epigraphRenderJson = {'<>':'div', "class":"quote_env", 'html':[  
        {'<>': "div", "class": "quote", "html": [
            {"[]": function(obj, index){return getJson2htmlComponentName(obj.epigraph, index)}, 'obj':function(){return(this.epigraph)}}                   

        ]}
    ]}



    json2html.component.add("line", lineRenderJson)
    json2html.component.add("verse", verseRenderJson)
    json2html.component.add("chapter", chapterRenderJson)
    json2html.component.add("epigraph", epigraphRenderJson)


    const template = {"<>":"html","html": [	
        getHtmlHeader(stylesheet),
        {"<>":"body","html": [
            {"<>":"div","class":"center", "html": [
                {"<>":"div","class":"poem", "html": [
                    {"<>":"H1", "class": "poem_title", "text": "${title}"},
                    /* DEVNOTE: the intention of tthe following code is to only execute if epigraph exists, 
                    but I'm not 100% sure this is exactly what happens here */
                    {"[]": function(obj, index){ 
                        if(obj.epigraph){
                            return "epigraph"
                        }
                    }, "obj": "$this.epigraph"},
                    {"[]": function(obj, index){return getJson2htmlComponentName(obj.content, index)}, 'obj':function(){return(this.content)}}                    
                ]}
            ]}
        ]}

    ]};
    
    const uglyRes = json2html.render(poem,template)
    const res = beautify(uglyRes)
    return res
}

function getJson2htmlComponentName(content, index){
    /* DEVNOTER: content contains the list of sub-json objects at the current level, while index identifies its index in it
    See e.g. http://www.json2html.com/docs/#main-assignment-shorthand (or ctrl-f the page for 'index')
    */
    //ToDo store this in other location (and consider trying to generate dynamically somehow)
    mapOfValidationFunctionsToRenderers = [
        [validateLine, "line"],
        [validateVerse, "verse"],
        [validateChapter, "chapter"]
    ]
    if(index >= 0){ //If not within an arary, this value is -1 as per json2html docs
        renderFunction = getResForFirstMatchingValidationFunction(content[index], mapOfValidationFunctionsToRenderers)
    }
    else{
        renderFunction = getResForFirstMatchingValidationFunction(content, mapOfValidationFunctionsToRenderers)
    }

    //console.log(index)
    //console.log("Content: ", JSON.stringify(content, null, 1))
    return renderFunction
}


async function renderHtml(work, client){
    // ToDo Think on where to put the logic of creating this mapping
    mapOfValidationFunctionsToStyles = [
        [validatePoem, renderPoemHtml],
        [validateCollection, renderCollectionHtml]
    ]

    const styleFunction = getResForFirstMatchingValidationFunction(work, mapOfValidationFunctionsToStyles)
    res = await styleFunction(work, client)

    return res
}


function getResForFirstMatchingValidationFunction(jsonWork, mapOfValidationFunctionsToRes){
    /*The map of schemas to res should be ordered from most specific to most general, if a work can match multiple schemas
        the first match is returned
    Note: it is assumed to be a list of list, with the inner list having length 2 (i0: schemavalidation function, i1: html render function)
    */
    for (let i = 0; i < mapOfValidationFunctionsToRes.length; i++) {
        const validationFunction = mapOfValidationFunctionsToRes[i][0]        
        try{
            schemas.validateSchema(jsonWork, validationFunction)
            return mapOfValidationFunctionsToRes[i][1]
        }
        catch(error){}//No problem: simply try the next
        
    }    
    throw new NoValidatingFunctionFoundError(`No matching style found for object ${JSON.stringify(jsonWork, null, 1)} (Validation functions tried: ${mapOfValidationFunctionsToRes})`)
}


async function getPoemStyle(){
    const poemCss = await fs.readFile(POEM_STYLESHEET_LOC)
    return poemCss.toString()
}

async function getCollectionStyle(){
    const poemCss = await fs.readFile(COLLECTION_STYLESHEET_LOC)
    return poemCss.toString()
}

module.exports.renderHtml = renderHtml
module.exports.STANDARD_POEM_STYLESHEET = STANDARD_POEM_STYLESHEET
module.exports.STANDARD_COLLECTION_STYLESHEET = STANDARD_COLLECTION_STYLESHEET
module.exports.getPoemStyle = getPoemStyle
module.exports.getCollectionStyle = getCollectionStyle
module.exports.NoMatchingStyleFoundError = NoValidatingFunctionFoundError
