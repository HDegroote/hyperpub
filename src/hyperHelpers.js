const Hyperdrive = require('hyperdrive')
const workSchema = require("./schemas/workSchema")


const WORK_JSON_LOC_IN_DRIVE = "/work.json"
const WORK_HTML_LOCATION_IN_DRIVE = "/index.html"



//const = require("./renderHtml")
//const { create_work, read_work_as_json } = require("./create_work")


async function addHyperdriveToClient(client){
    const drive = new Hyperdrive(client.corestore(), null) 
    await drive.promises.ready()
    await client.replicate(drive) //Seed
    return drive
}

async function getHyperdriveFromClient(client, key){
    const drive = new Hyperdrive(client.corestore(), Buffer.from(key, "hex"))    
    // DEVNOTE: this throws Error: RPC stream destroyed if the hyperspace daemon is not running
    await drive.promises.ready()
    await client.replicate(drive) //Sync and seed
    return drive
}

function check_is_valid_json_obj(json_obj){
    /* Throws error if the json object is not valid, else returns nothing */
    const json_as_string = JSON.stringify(json_obj)
    if(json_as_string.length < 2 || json_as_string[0] != "{" || json_as_string.slice(-1) != "}"){
        const error_str = `Attempting to write an invalid json object: does not start and end with '{' and '}' (stringifies to '${json_as_string}'`
        throw Error(error_str)
    }
}


async function writeJsonToDrive(json_obj, drive, location){
    check_is_valid_json_obj(json_obj)
    const json_as_string = JSON.stringify(json_obj)
    if(json_as_string.length < 2 || json_as_string[0] != "{" || json_as_string.slice(-1) != "}"){
        const error_str = `Attempting to write an invalid json object: does not start and end with '{' and '}' (stringifies to '${json_as_string}'`
        throw Error(error_str)
    }
    await drive.promises.writeFile(location, json_as_string)
    //console.log("Added the updated json file of the work to the new hyperdrive with key ", key,
    //    `Json content: ${work_as_string}`)
}


async function read_json_from_drive(drive, location){
    const read_file = await drive.promises.readFile(location, 'utf8')
    const json_content = JSON.parse(read_file.toString())
    return json_content
}


//ToDo restructure project so this is with the work creator
async function read_work_as_json(key, client, work_location_in_drive = WORK_JSON_LOC_IN_DRIVE) {
    //console.log("Getting drive with key", key)
    drive = await getHyperdriveFromClient(client, key);
    //console.log("reading content of drive with key", key)
    json_content = await read_json_from_drive(drive, work_location_in_drive);
    workSchema.validateWork(json_content);
    return json_content;
}

module.exports = {
    "addHyperdriveToClient": addHyperdriveToClient,
    "getHyperdriveFromClient": getHyperdriveFromClient,
    "writeJsonToDrive": writeJsonToDrive,
    "read_json_from_drive": read_json_from_drive,
    "check_is_valid_json_obj": check_is_valid_json_obj,
    "read_work_as_json" : read_work_as_json,
    "WORK_JSON_LOC_IN_DRIVE": WORK_JSON_LOC_IN_DRIVE,
    "WORK_HTML_LOCATION_IN_DRIVE": "/index.html"
}
