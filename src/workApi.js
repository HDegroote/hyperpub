const workSchema = require("./schemas/workSchema");
const renderHtml = require("./renderHtml");
const hyperHelpers = require("./hyperHelpers");
const {NoMatchingStyleFoundError} = require("./renderHtml")
const collectionSchema = require("./schemas/collectionSchema")

function checkDoesNotContainHashField(jsonObj){
    /* Throws error if the json object is not valid, else returns nothing */
    if(workSchema.HASH_KEY in jsonObj){
        const error_str = `The given object already contains a hash field: ${jsonObj}`
        throw Error(error_str)
    }
}


async function create_work(
    workJson,
    client,
    jsonLocationInDrive = hyperHelpers.WORK_JSON_LOC_IN_DRIVE,
    htmlLocationInDrive = hyperHelpers.WORK_HTML_LOCATION_IN_DRIVE
) {
    /* ToDo Consider splitting up this method*/    
    hyperHelpers.check_is_valid_json_obj(workJson);
    checkDoesNotContainHashField(workJson);
    const drive = await hyperHelpers.addHyperdriveToClient(client);
    const key = drive.key.toString("hex");

    const workWithHash = Object.assign({}, workJson); //ToDo evaluate if this deep-copy method suffices
    workWithHash[workSchema.HASH_KEY] = key;

    await hyperHelpers.writeJsonToDrive(workWithHash, drive, jsonLocationInDrive);

    let htmlPageTemp
    try{
        htmlPageTemp = await renderHtml.renderHtml(workWithHash, client); //ToDo add test for collection html (which required client)
    }catch(e){
        if (e instanceof NoMatchingStyleFoundError) {            
            console.warn(`Could not generate HTML for work ${workJson} because no matching style was found -- generating standard error page`)           
            htmlPageTemp = "<HTML><BODY><H3>Could not generate HTML for this work html --was a format function defined for this type of work? </H3></BODY></HTML>"
        } else {
            throw e;  
        }
    }
    const htmlPage = htmlPageTemp
    await drive.promises.writeFile(htmlLocationInDrive, htmlPage);

    //ToDo don't add all style files always (link with hyperhashes?)
    const poemStyle = await renderHtml.getPoemStyle()
    await drive.promises.writeFile(renderHtml.STANDARD_POEM_STYLESHEET, poemStyle)
    const collectionStyle = await renderHtml.getCollectionStyle()
    await drive.promises.writeFile(renderHtml.STANDARD_COLLECTION_STYLESHEET, collectionStyle)

    //console.log("Created new work in drive with key:", key, "and content:", JSON.stringify(workWithHash));
    return key;
}

async function createWorks(
    workJsons, 
    client,
    jsonLocationInDrive = hyperHelpers.WORK_JSON_LOC_IN_DRIVE,
    htmlLocationInDrive = hyperHelpers.WORK_HTML_LOCATION_IN_DRIVE
){
    const addedHashes = []
    for(const workJson of workJsons){
        const addedHash = await create_work(workJson, client, jsonLocationInDrive, htmlLocationInDrive)
        addedHashes.push(addedHash)
    }
    return addedHashes
}

async function createCollectionAndWorksInIt(
    collectionName, 
    workJsons,
    client,
    jsonLocationInDrive = hyperHelpers.WORK_JSON_LOC_IN_DRIVE,
    htmlLocationInDrive = hyperHelpers.WORK_HTML_LOCATION_IN_DRIVE
){
    /* Helper function to create a collection of new works; creates both all works and the collection containing them*/
    //const workHashes = await ownCollectionApi.addWorksToOwnCollection(workJsons, client)
    const workHashes = await createWorks(workJsons, client, jsonLocationInDrive, htmlLocationInDrive)
    const jsonCollection = collectionSchema.createCollectionFromTitleAndWorkHashes(collectionName, workHashes)       
    const collectionHash = await create_work(jsonCollection, client, jsonLocationInDrive, htmlLocationInDrive)
    workHashes.push(collectionHash)
    return workHashes
}


module.exports.create_work = create_work;
module.exports.createCollectionAndWorksInIt = createCollectionAndWorksInIt
module.exports.createWorks = createWorks

