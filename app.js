const parseTxt = require("./src/parseTxt")
const { Client: HyperspaceClient } = require('hyperspace')
const ownCollectionApi = require("./src/ownCollectionApi")
const readline = require('readline');
const hyperHelpers = require('./src/hyperHelpers');
const { promises: fs } = require("fs");
const path = require("path")
const workApi = require("./src/workApi");


const client = new HyperspaceClient()
const interface = readline.createInterface( process.stdin, process.stdout );

// From https://stackoverflow.com/questions/8128578/reading-value-from-console-interactively
var readQuestion = function(questionTxt, interface) {
    return new Promise( (res, rej) => {
        interface.question( questionTxt, answer => {
            res(answer);
        })
    });
};

async function runGetOwnCollectionFlow(){
    const ownCollection = await ownCollectionApi.getOwnCollection(client)
    console.log(ownCollection.title)
    console.log("-----------------------------------------------------")
    for(workHash of ownCollection.works){
        work = await hyperHelpers.read_work_as_json(workHash, client)
        console.log("-", work.title, `(${workHash})`)
    }
    console.log("")
}

async function runAddWorkFlow(){
    const addTxt = `Enter the location of the .txt work to be added: `
    const location = await readQuestion(addTxt, interface);     
    const poemTxt = await fs.readFile(location, 'utf8')
    const jsonPoem = parseTxt.parseTxtPoem(poemTxt)       
    const hash = await ownCollectionApi.addWorksToOwnCollection([jsonPoem], client)
    console.log("Successfully added work: ", hash)
}


async function readAndParseWorksInDirectory(directory){
    const files = await fs.readdir(directory)
    const worksAsJsons = []
    for(file of files){ 
        console.log("Processing", file)
        //ToDo make generic, not just for poems
        const poemTxt = await fs.readFile(path.join(directory, file), 'utf8')
        const jsonPoem = parseTxt.parseTxtPoem(poemTxt)   
        worksAsJsons.push(jsonPoem)
    }
    return worksAsJsons
}


async function runAddCollectionWorkflow(){
    const addTxt = `Enter the location of the .txt collection to be added: `
    const location = await readQuestion(addTxt, interface);   
    const collectionTxt = await fs.readFile(location, 'utf8')
    const jsonCollection = parseTxt.parseTxtCollection(collectionTxt)       
    const hash = await ownCollectionApi.addWorksToOwnCollection([jsonCollection], client)
    console.log("Successfully created a new collection and added it: ", hash)
}


async function runAddCollectionFromDirWorkflow(){
    const questionTxt = `Enter the location of the directory describing the new collection (The directory name will be the collection name, ` +
        `and its content will make up the works): `
    const locationInput = await readQuestion(questionTxt, interface);     
    const location = path.resolve(locationInput) //DEVNOTE: should probably make an explicit check for existence, to have better error messages
    const collectionName = path.basename(location)
    const worksAsJsons = await readAndParseWorksInDirectory(location)
    const workHashes = await workApi.createCollectionAndWorksInIt(collectionName, worksAsJsons, client)
    await ownCollectionApi.addHashesToOwnCollection(workHashes, client)
    console.log("Successfully created a new collection with the works from ", location, `(${workHashes.slice(-1)})`) //ToDo don't get hash of collection so ad ho
}


async function main() {
    const showCommand = "show"
    const addWorkCommand = "add"
    const addCollectionCommand = "col"
    const readCollectionFromDirCommand = "fromDir"
    const questionTxt = `Options: \n\t-'${showCommand}' to show all works\n\t-'${addWorkCommand}' to add a new work \n\t-'${addCollectionCommand}'` +
    ` to add a new collection \n\t-'${readCollectionFromDirCommand}' to create a new collection from a directory\n\t(leave blank to exit)\ninput: `

    let answer;
    while ( answer != '' ) {
        answer = await readQuestion(questionTxt, interface);
        if(answer == showCommand){
            await runGetOwnCollectionFlow()
        }        
        else if(answer == addWorkCommand){
            await runAddWorkFlow()
        }
        else if(answer == addCollectionCommand){
            await runAddCollectionWorkflow()
        }
        else if(answer == readCollectionFromDirCommand){
            await runAddCollectionFromDirWorkflow()
        }
        else{
            if(answer != ""){
                console.log("Invalid command -- please try again")
            }
        }
    }
    console.log('exiting interface');
    interface.close();    
    console.log("Closing hyper client")
    await client.close()    
}

/*
async function test(){
    ownCollection = await ownCollectionApi.getOwnCollection(client)
    console.log("Own collection at start: ", JSON.stringify(ownCollection))

    poemTxt1 = `Ode to self-publication

    I wish I were well-versed in verse
    But this is my first creation
    Yet for better or for worse
    I pursue its publication
    `

    poemTxt2 = `This is another poem
    
    With a two-line verse
    Right here

    And another verse
    Right at the end
    `
    const txtPoems = [poemTxt1, poemTxt2]
    const jsonPoems = []
    for(const txtPoem of txtPoems){
        jsonPoem = parseTxt.parseTxtPoem(txtPoem)
        jsonPoems.push(jsonPoem)
    }

    await ownCollectionApi.addWorksToOwnCollection(jsonPoems, client)

    //await addWorksToOwnCollection(jsonPoems, client)

    console.log("closing client")
    client.close();
}

*/

/*
async function setupCollectionOfAllOwnWorks(corestore){    
    let key
    try {
        file = await fs.readFile(keyOfOwnCollectionLocation, 'utf8')
        key = file.toString()
    } catch (err) {
        const msg = `Warning: could not load the collection of own works from the expected location ${keyOfOwnCollectionLocation}. ` +
        "Will create a new drive to store it and save the key there (original error: '" + err.toString() + "'"
        console.warn(msg)
        const newOwnCollection = collectionSchema.createCollectionFromTitleAndWorkHashes("All works created by the owner of this hyperspace instance", [])
        key = await hyperHelpers.create_work(newOwnCollection, corestore)
        fs.writeFile(keyOfOwnCollectionLocation, key)  
    }
    ownWorksCollectionJson = await hyperHelpers.read_work_as_json(key, corestore)
    ownWorksCollection = new collectionSchema.Collection(ownWorksCollectionJson)
    console.log(ownWorksCollection)
}*/

/*
async function main(){
    await setupCollectionOfAllOwnWorks(client.corestore());
    /*drive = await hyperHelpers.getHyperdriveFromCorestore(client.corestore(), ownKey)
    console.log(drive)

    if(!drive.writable){
        //ToDo add check that it's not a new empty drive
        throw Error("The drive with all your works is not writable from the running hyperspace client, which means it was not created with. "+
            `Please confirm that the correct hyperspace client is running. (key: ${drive.key}`);
    }

    console.log("writable", drive.writable)
    
}*/

main()