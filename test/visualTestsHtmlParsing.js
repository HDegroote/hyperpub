/* These are not unit tests, but tests where you should look at the rendered HTML to verify it is correct
This is done because unit testing HTML generation is annoying, and it is not part of the core system*/
const parseTxt = require("../src/parseTxt")
const renderHtml = require("../src/renderHtml")
const { promises: fs } = require("fs");
const path = require("path")
const poemSchema = require("../src/schemas/poemSchema")

function getHtmlForSimplePoemExample(){
    const poem = `A simple poem

This is a simple poem
Of three verses

This is the second verse

And this is its end`

    jsonPoem = parseTxt.parseTxtPoem(poem)
    //console.log(JSON.stringify(jsonPoem, null, 2))
    htmlPoem = renderHtml.renderHtml(jsonPoem)
    return htmlPoem
}


function getHtmlForPoemWithChaptersExample(){
    const poem = `A chaptered poem
    
Chapter 1

This is a chaptered poem
Although it's not long

This is the second verse of the first chapter
\tSee this line be indented?
\t\tAnd this one even more?


Chapter 2

This is the second chapter
It has only a single verse
`

    let jsonPoem = parseTxt.parseTxtPoem(poem)

    const epigraph = poemSchema.createLineFromStr("This is an epigraph")
    jsonPoem = poemSchema.addOrReplaceEpigraphToPoem(jsonPoem, epigraph)
    //console.log("chaptered poem: ", JSON.stringify(jsonPoem))
    htmlPoem = renderHtml.renderHtml(jsonPoem)
    return htmlPoem
}


async function main(){
    const resDir = "./visualTestsRes"    
    try{    
        await fs.mkdir(resDir)
    }catch(e){
        console.log("Not creating directory ", resDir, ' since it already exists')
    }

    const simplePoemDest = path.join(resDir, "simplePoem.html")
    const chapteredPoemDest = path.join(resDir, "chapteredPoem.html")
    const simpleHtmlPoem = await getHtmlForSimplePoemExample()    
    await fs.writeFile(simplePoemDest, simpleHtmlPoem)
    const chapteredPoem = await getHtmlForPoemWithChaptersExample()
    await fs.writeFile(chapteredPoemDest, chapteredPoem)


    const poemStyle = await renderHtml.getPoemStyle()
    const poemStyleLoc = path.join(resDir, renderHtml.STANDARD_POEM_STYLESHEET)
    await fs.writeFile(poemStyleLoc, poemStyle)

}

main()