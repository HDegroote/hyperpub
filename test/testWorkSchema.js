
const Ajv = require("ajv")
const chai = require("chai");
const expect = chai.expect;
poemSchemas = require("../src/schemas/poemSchema")
workSchema = require("../src/schemas/workSchema")



describe("Test work schema", function() {
    const hypercoreRegex = workSchema.getHypercoreHashRegex()
    const aHash = "a".repeat(64)
    it("Check hypercore regex true positives", function() {
      const valids = [
          "hyper://" + "a".repeat(64) +"/",
          "hyper://" + "a".repeat(64) +"",
          "a".repeat(64)
      ]
      for(let valid of valids){
          //console.log(valid)
          //console.log(valid.match(hypercoreRegex))
          expect(valid.match(hypercoreRegex)).to.be.not.null
      }
    
    });
  
    it("Check hypercore regex true negatives", function() {
      const invalids = [
          "hyper://" + "a".repeat(63) +"/",
          "hyper://" + "a".repeat(65) +"/",
          "hyper://" + "z".repeat(64) +"/",
          "a.hyper://" + "z".repeat(64) +"/",
          "hyper://" + "z".repeat(64) +"/no"
      ]  
      for(let invalid of invalids){
          expect(invalid.match(hypercoreRegex)).to.be.null
      }
      
    });
  
    it("Check minimal schema is valid", function() {
      const exWorkJson = {
          "hash": aHash
      }
  
      isValid = workSchema.validateWork(exWorkJson)
      expect(isValid).to.be.true 
    });
  
    it("Check invalid schema throws error", function() {
      const exWorkJson = {
          "hash": "notAHash"
      }
      expect(() => workSchema.validateWork(exWorkJson)).to.throw()  
    });
  
    it("Check arbitrary with at least hash is valid", function(){
      const exWorkJson = {
          "hash": aHash,
          "title": "A title",
          "soemthingRandom": "Nobody cares"
      }
  
      isValid = workSchema.validateWork(exWorkJson)
      expect(isValid).to.be.true 
    });
  
    it("Check getWorkHashRegex works as expected", function(){
      //DEVNOTE: the test is mainly to ensure the location in the schema is correct, hence the very exact check. 
      //  If ever the regex is changed, this test will also need to be updated
      expect(workSchema.getHypercoreHashRegex()).to.equal("^(hyper://)?([a-f0-9]{64})/?$")
    });
  
  });
  
  