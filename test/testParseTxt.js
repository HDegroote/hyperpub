const chai = require("chai");
const expect = chai.expect;
const nodeAssert = require('assert/strict');
const parser = require("../src/parseTxt")
const poemSchema = require("../src/schemas/poemSchema")
const hyperHelpers = require("../src/hyperHelpers")
const workApi = require("../src/workApi")


describe("Test poemTxt parser", function(){

    it("Check parseTxtPoem normal execution (no chapters)", function(){

        const title = "A poem"
        const line0 = "This is a poem"
        const line1 = "Of one verse"
        const line2 = "And three lines"
        const txtPoem = title + "\n\n" + line0 + "\n" + line1 + "\n" + line2
        const poem = parser.parseTxtPoem(txtPoem)
        expect(poem.title).to.equal(title)
        expect(poem.content.length).to.equal(1)
        //ToDo think of way to do these checks without going so much in internals (extend classes?)
        const verse = poem.content[0][poemSchema.Verse.VERSE_KEY]

        expect(verse.length).to.equal(3)
        expect(verse[0].text).to.equal(line0)
        expect(verse[1].text).to.equal(line1)
        expect(verse[2].text).to.equal(line2)

    });

    it("Check parseTxtPoem: ensure spaces fore and aft removed", function(){
        const txtPoem = `   ugly spaces
        
              everywhere    
              everyhere`
        const poem = parser.parseTxtPoem(txtPoem)
        expect(poem.title).to.equal("ugly spaces")
        expect(poem.content.length).to.equal(1)
        //ToDo think of way to do these checks without going so much in internals (extend classes?)
        const verse = poem.content[0][poemSchema.Verse.VERSE_KEY]

        expect(verse.length).to.equal(2)
        expect(verse[0].text).to.equal("everywhere")
        expect(verse[1].text).to.equal("everyhere")
    });

    it("Check parseTxtPoem normal execution (with chapters)", function(){
        const poemTitle = "A poem"
        const ch1Title = "Chapter 1"
        const ch2Title = "Chapter 2"
        const line0 = "This is a poem"
        const line1 = "Of two chapters"
        const line2 = "And three lines"
        const txtPoem = poemTitle + "\n\n" + ch1Title + "\n\n" + line0 + "\n" + line1 + "\n\n\n" + ch2Title + "\n\n" + line2
        const poem = parser.parseTxtPoem(txtPoem)
        expect(poem.title).to.equal(poemTitle)

        //ToDo think of way to do these checks without going so much in internals (extend classes?)
        expect(poem.content.length).to.equal(2)
        const ch1 = new poemSchema.Chapter(poem.content[0])
        expect(ch1.title).to.equal(ch1Title)
        expect(ch1.content.length).to.equal(1)
        const ch1Verse1 = new poemSchema.Verse(ch1.content[0])
        expect(ch1Verse1.lines.length).to.equal(2)
        expect(new poemSchema.Line(ch1Verse1.lines[0]).text).to.equal(line0)
        expect(new poemSchema.Line(ch1Verse1.lines[1]).text).to.equal(line1)
        
        const ch2 = new poemSchema.Chapter(poem.content[1])
        expect(ch2.title).to.equal(ch2Title)
        expect(ch2.content.length).to.equal(1)
        const ch2Verse1 = new poemSchema.Verse(ch2.content[0])
        expect(ch2Verse1.lines.length).to.equal(1)
        expect(new poemSchema.Line(ch2Verse1.lines[0]).text).to.equal(line2)
    });

    it("Check parseTxtPoem: ensure blank lines with spaces do not cause issues when parsing chaptered poem", function(){
        const txtPoem = `   ugly spaces
          
           Chapter 1

              everywhere    
              everyhere


              Chapter 2

              And here
                Too
              `
        const poem = parser.parseTxtPoem(txtPoem)
        expect(poem.title).to.equal("ugly spaces")
        expect(poem.content.length).to.equal(2)
        //ToDo think of way to do these checks without going so much in internals (extend classes?)
        const ch1 = new poemSchema.Chapter(poem.content[0])
        const ch2 = new poemSchema.Chapter(poem.content[1])

        //If ever error here: add deeper tests for line content as well
    });

    it("Check parseTxtPoem: can identify tab/indent for first line", function(){
        const txtPoem = "The Poem\n\n\tTabbed first line\nIs it indented?"
        const poem = parser.parseTxtPoem(txtPoem)
        const verse = new poemSchema.Verse(poem.content[0])
        const line = new poemSchema.Line(verse.lines[0])

        //ToDo think new Verse(of way to do these checks without going so much in internals (extend classes?)
        console.log("line 1", line)
        expect(line.indent).to.equal(1)
    })

    it("Check helper getNrOfTabsBeforeIndex works correctly",function(){
        const line = "  \t\tThis is it"
        const startI = 4
        expect(line[startI]).to.equal("T") //Sanity check for test itself
        const nrTabs = parser.getNrOfTabsBeforeIndex(line, startI)
        expect(nrTabs).to.equal(2)
      });

    it("Check helper splitInTitleAndBody throws error when no blank line after title", function(){
        const text = "Title\nNo Blank line here\nSo error should be thrown"
        expect(() => parser.splitInTitleAndBody(text)).to.throw()
    })

    it("Check parseLine handles tabs", function(){
        const line = "\t\tdouble tab"
        const parsedLine = new poemSchema.Line(parser.parseLine(line))
        expect(parsedLine.indent).to.equal(2)
    });

});

describe("Test collection Txt parsers", async function(){
    let client
    const collectionTitle = "My collection"
    let txtCollection

    beforeEach(async function () {  
        client = this.localClient
        work1Key = await workApi.create_work({"title": "work1"}, client)
        txtCollection = `${collectionTitle}
    
    ${work1Key}
    `
    });

    
    it("Check parseTxtCollection normal execution", function(){
        const collection = parser.parseTxtCollection(txtCollection)
        expect(collection.title).to.equal(title)
        expect(collection.works).to.deep.equal([work1Key])
        //ToDo think of way to do these checks without going so much in internals (extend classes?)
        

        
    });
});


