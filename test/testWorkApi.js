const helpers = require("../src/hyperHelpers")
const workSchema = require("../src/schemas/workSchema")
//const simulator = require('hyperspace/simulator')
const chai = require("chai");
const expect = chai.expect;
const nodeAssert = require('assert/strict');
const workApi = require("../src/workApi")




describe("Test work API", async function() {
    let client;

    beforeEach(function () {  
        client = this.localClient
    });


    let jsonObj
    const jsonLocInDrive = "work.json"
    const htmlLocInDrive = "index.html"

    const titleKey = workSchema.TITLE_KEY 
    const hashKey = workSchema.HASH_KEY
    const titleValue = "The title"

    beforeEach(async function () {            
        jsonObj = {[titleKey]: titleValue}            
    });

    //ToDo Figure out what exactly happens with client variable in this context
    async function helper_ensureDriveContainsJsonAndHtmlFile(driveHash){
        const drive = await helpers.getHyperdriveFromClient(client, driveHash)
        const jsonFileContent = await drive.promises.readFile(jsonLocInDrive, 'utf8')
        expect(jsonFileContent.length).to.be.gt(0) //DEVNOTE: pretty useless test, as simply not throwing an exception upon reading suffices to ensure this file exists
        const htmlFileContent = await drive.promises.readFile(htmlLocInDrive, "utf-8")
        expect(htmlFileContent.length).to.be.gt(0) //DEVNOTE: same as above
    }
    

    

    it("Check create_work standard execution", async function() {
        key = await workApi.create_work(jsonObj, client, jsonLocInDrive, htmlLocInDrive)
        drive = await helpers.getHyperdriveFromClient(client, key)
        file = await drive.promises.readFile(jsonLocInDrive, 'utf8')
        recovered_json = JSON.parse(file.toString())

        gold = {
            [hashKey]: key, // https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Object_initializer#computed_property_names
            [titleKey]: titleValue
        }
        expect(recovered_json).to.deep.equal(gold)

        htmlFile = await drive.promises.readFile(htmlLocInDrive)
        expect(htmlFile.toString().length).to.be.gt(0)
        

    });

    it("Check create_work from invalid json throws error", async function() {
        await nodeAssert.rejects(workApi.create_work("not json", client, jsonLocInDrive))
    });

    it("Check create_work from a json which already contains a hash field throws error", async function() {
        jsonObj[hashKey] = "a".repeat(64)
        await nodeAssert.rejects(workApi.create_work(jsonObj, client, jsonLocInDrive))
    });
    
    
    it("Test createWorks standard execution", async function(){
        const title2 = "Title 2"
        const jsonObj2 = {[titleKey]: title2 }            
        const works = [jsonObj, jsonObj2]
        const addedHashes = await workApi.createWorks(works, client, jsonLocInDrive, htmlLocInDrive)
        for(hash of addedHashes){
            await helper_ensureDriveContainsJsonAndHtmlFile(hash)
        }
    })
    
    
    it("Test createCollectionAndWorksInIt standard execution", async function(){
        const collectionTitle = "collection"
        const works = [jsonObj]
        addedHashes = await workApi.createCollectionAndWorksInIt(collectionTitle, works, client)
        for(hash of addedHashes){
            await helper_ensureDriveContainsJsonAndHtmlFile(hash)
        }
    })

});

