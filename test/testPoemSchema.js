const chai = require("chai");
const { Line, Verse, Poem, Chapter } = require("../src/schemas/poemSchema");
const expect = chai.expect;
const schemas = require("../src/schemas/poemSchema")



describe("Test poem schema", function() {
    let aLine, aLine2
    let aVerse, aChapter
    let exPoemJson
    const aHash = "a".repeat(64)

    beforeEach(function () {
      aLine = {"line": {"text": "line 1"}}
      aLine2 = {"line": {"text": "line 2"}}
      aVerse = {"verse": [aLine]}
      aChapter = {[Chapter.CHAPTER_KEY]: {[Chapter.CONTENT_KEY]: [aVerse]}}
      exPoemJson = new Poem({
        "hash": aHash,
        "title": "A title",
        "poem":{
          "content": [
            aVerse
          ]
        }
    })

    })
      
    it("Check simple poem schema validates", function() {    
      isValid = schemas.validatePoem(exPoemJson)
      expect(isValid).to.be.true 
    });

    it("Check detects invalid line in poem schema", function() {
      console.log(JSON.stringify(exPoemJson, "  "))
      exPoemJson.content[0].verse[0].line = {"noText": "line 1"}
      console.log(JSON.stringify(exPoemJson, "  "))
      //schemas.validatePoem(exPoemJson)
      //schemas.validateVerse(exPoemJson)

      expect(() => schemas.validatePoem(exPoemJson)).to.throw()
    });

    
    it("Check detects invalid poem content type", function() {
        exPoemJson.content[0] = {"invalidContentType": [
          {"line": {"noText": "line 1"}}
      ]}
      expect(() => schemas.validatePoem(exPoemJson)).to.throw()
    });

    it("Check accepts verse as poem content", function() {
      exPoemJson.content.push(aVerse)      
      isValid = schemas.validatePoem(exPoemJson)
      expect(isValid).to.be.true 
  });

 
  it("Check detects invalid verse (no lines)", function() {
    exPoemJson.content.push({"verse": []})      
    expect(() => schemas.validatePoem(exPoemJson)).to.throw() 
  });
   
  it("Check detects invalid verse (invalid line)", function() {
    exPoemJson.content.push({"verse": [
      {"noLine": {"text": "no line 1"}}
    ]})
    expect(() => schemas.validatePoem(exPoemJson)).to.throw() 
  });

  
  it("Check accepts chapter as poem content", function() {
    exPoemJson.content.push(aChapter)
    isValid = schemas.validatePoem(exPoemJson)
    expect(isValid).to.be.true 
  });

  
  it("Check chapter can contain verses as content", function() {
    aChapter.chapter.content.push(aVerse)
    exPoemJson.content.push(aChapter)
    console.log("poem", JSON.stringify(exPoemJson))
    console.log(JSON.stringify(exPoemJson, "  "))
    isValid = schemas.validatePoem(exPoemJson)
    expect(isValid).to.be.true 
  });

  
  it("Check detects invalid chapter (no content)", function() {
    aChapter.chapter = []
    exPoemJson.content.push(aChapter)
    expect(() => schemas.validatePoem(exPoemJson)).to.throw() 
  });

  
  describe("Test poem parsing", function(){
    const aTitle = "A title"    
    let aContent 
  
    beforeEach(function () {
      aContent = [aVerse]
    })

    it("Check createPoemFromTitleAndContent normal execution", async function() {
      const poem = schemas.createPoemFromTitleAndContent(aTitle, aContent)
      expect(schemas.validatePoem(poem)).to.be.true
      expect(poem[Poem.CONTENT_KEY]).to.deep.equal(aContent)
      expect(poem[Poem.TITLE_KEY]).to.equal(aTitle)
      expect(poem.epigraph).to.equal(null);
    }); 

    
    it("Check createPoemFromTitleAndContent not a valid content throws error", async function() {
      expect(() => schemas.createPoemFromTitleAndContent(aTitle, ["no proper content"])).to.throw() 
      expect(() => schemas.createPoemFromTitleAndContent(aTitle, "no proper content")).to.throw() 
      expect(() => schemas.createPoemFromTitleAndContent(aTitle, aVerse)).to.throw() //Should be array
    }); 

    it("Check createPoemFromTitleAndContent not a valid title throws error", async function() {
      expect(() => schemas.createPoemFromTitleAndContent(1, aContent)).to.throw() 
    }); 
    
    it("Check createPoemFromTitleAndContent normal execution with epitath", async function() {
      const epigraph = schemas.createLineFromStr("My epigraph")
      const poem = schemas.createPoemFromTitleAndContent(aTitle, aContent, epigraph)
      expect(poem[Poem.CONTENT_KEY]).to.deep.equal(aContent)
      expect(poem[Poem.TITLE_KEY]).to.equal(aTitle)
      expect(poem.epigraph).to.deep.equal(epigraph);

    }); 

  });
  
});

describe("Test line schema", function() {
  let aLine

  beforeEach(async function () {
    aLine = {"line": {"text": "line 1"}}
  })

  it("Check standard line schema validates", function() {   
      isValid = schemas.validateLine(aLine)
      expect(isValid).to.be.true 
  });

  it("Check line schema with non-string text fails validation", function() {   
    aLine["line"]["text"] = 1
    expect(() => schemas.validateLine(aLine)).to.throw() 
  });

  it("Check line schema without text field fails validation", function() {   
    aLine = {"line": {}}
    expect(() => schemas.validateLine(aLine)).to.throw() 
  });

  it("Check line schema without line field fails validation", function() {   
    aLine = {}
    expect(() => schemas.validateLine(aLine)).to.throw() 
  });

  it("Check get indent returns 0 if none defined", function(){
    const line = new Line(aLine);
    expect(line.indent).to.equal(0)
  });

  it("Check get indent returns correctly when defined", function(){
    aLine.line[Line.INDENT_KEY] = 2
    const line = new Line(aLine);
    expect(line.indent).to.equal(2)
  });


  describe("Test line parsing", function(){
    it("Check createLineFromStr normal execution", async function() {
      txt = "line text"
      line = schemas.createLineFromStr(txt)
      expect(schemas.validateLine(line)).to.be.true
      expect(line.text).to.equal(txt)
  
    }); 

    it("Check createLineFromStr not a string throws error", async function() {
      expect(() => schemas.create_line_from_str(1)).to.throw() 
    }); 

    it("Check createLineFromStr with indent", async function() {
      txt = "line text"      
      line = schemas.createLineFromStr(txt, 2)
      expect(schemas.validateLine(line)).to.be.true
      expect(line.text).to.equal(txt)
      expect(line.text).to.equal(txt)
      expect(line.indent).to.equal(2)
    }); 


  });
});


describe("Test verse schema", function() {
  let aLine
  let aVerse

  beforeEach(async function () {
    aLine = {[Line.LINE_KEY] : {[Line.TEXT_KEY]: "line 1"}}
    aVerse = {[Verse.VERSE_KEY] : [aLine]}
  })

  it("Check standard verse schema validates", function() {   
      isValid = schemas.validateVerse(aVerse)
      expect(isValid).to.be.true 
  });

  it("Check empty verse does not validate", function() {   
    aVerse[Verse.VERSE_KEY] = []
    expect(() => schemas.validateVerse(aVerse)).to.throw() 
});

  it("Check no verse does not validate", function() {   
    expect(() => schemas.validateVerse({})).to.throw() 
  });

  it("Check invalid line in verse does not validate", function() {   
    aVerse[Verse.VERSE_KEY].push("no line")
    expect(() => schemas.validateVerse({})).to.throw() 
  });
});
