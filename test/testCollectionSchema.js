const chai = require("chai");
const { Collection } = require("../src/schemas/collectionSchema");
const expect = chai.expect;
collectionSchema = require("../src/schemas/collectionSchema")

describe("Test collection schema", function() {
    const aHash = "a".repeat(64)
    const aTitle = "A title"
    let aCollection 

    beforeEach(function () {
      aCollection = {
          [Collection.TITLE_KEY]: aTitle, 
        [Collection.COLLECTION_KEY]: {[Collection.WORKS_KEY]: [aHash]}}
    })
      
    it("Check validateCollectionSchema standard execution", function() {    
        isValid = collectionSchema.validateCollection(aCollection)
        expect(isValid).to.be.true 
    });

    it("Check validateCollectionSchema fails with invalid works (hash instead of array of hashes)", function() {  
        aCollection[Collection.WORKS_KEY] = aHash
        expect(() => collectionSchema.validateCollection(exWorkJson)).to.throw()  
    });

    it("Check validateCollectionSchema fails with invalid title", function() {  
        aCollection[Collection.TITLE_KEY] = 1
        expect(() => collectionSchema.validateCollection(exWorkJson)).to.throw()  
    });

    
    describe("Test parsing of collection", function(){
        it("Test createCollectionFromTitleAndWorkHashes standard execution", function(){
            const anotherHash = "b".repeat(64)
            const works = [aHash, anotherHash]
            collection = collectionSchema.createCollectionFromTitleAndWorkHashes(aTitle, works)
            expect(collection.title).to.equal(aTitle)
            expect(collection.works).to.deep.equal(works)    
        });
  
        it("Test createCollectionFromTitleAndWorkHashes throws error when invalid hash", function(){
            const invalidHash = "b".repeat(63)
            const works = [aHash, invalidHash]
            expect(() => collectionSchema.createCollectionFromTitleAndWorkHashes(aTitle, works)).to.throw()
        });

        it("Test createCollectionFromTitleAndWorkHashes throws error when invalid title", function(){
            const works = [aHash]
            const title = 1
            expect(() => collectionSchema.createCollectionFromTitleAndWorkHashes(title, works)).to.throw()
        });

        it("Test createCollectionFromTitleAndWorkHashes may have no works", function(){
            const works = []
            collection = collectionSchema.createCollectionFromTitleAndWorkHashes(aTitle, works)
            expect(collection.title).to.equal(aTitle)
            expect(collection.works).to.deep.equal(works)    
        });




      });
});
