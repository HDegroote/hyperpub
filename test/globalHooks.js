//const simulator = require('hyperspace/simulator')
const { Client, Server} = require('hyperspace')
const fs = require('fs').promises


const simulatedRemoteHyperDriveStorage = './.temp-hyperspace-simulated-remote-storage'
const simulatedRemoteHostname = "simulated-remote-server"

const simulatedLocalHyperDriveStorage = './.temp-hyperspace-simulated-local-storage'
const simulatedLocalHostname = "simulated-local-server"


// See https://mochajs.org/#root-hook-plugins
exports.mochaHooks = async () => {
    console.log("Global mocha hook: setting up simulated remote server")
    const remoteServer = new Server({
        storage: simulatedRemoteHyperDriveStorage,
        host: simulatedRemoteHostname
      })      

    console.log("Global mocha hook: setting up simulated local server")
    const localServer = new Server({
        storage: simulatedLocalHyperDriveStorage,
        host: simulatedLocalHostname
      })      

    await localServer.ready()   
    await remoteServer.ready()

    const remoteClient = new Client({
        host: simulatedRemoteHostname
    })
    const localClient = new Client({
        host: simulatedLocalHostname
    })

    
    console.log("Defining global hooks")
    return {
        beforeEach(){
            this.remoteServer = remoteServer
            this.remoteClient = remoteClient
            this.localServer = localServer
            this.localClient = localClient
        },
        async afterAll(){
            console.log("Global mocha hook: shutting down simulated local server")
            //await localServer.close()
            localServer.close() //Should await, but takes >10s. Just assume it will close eventually

            console.log(`Global mocha hook: removing the hyperspace files from ${simulatedLocalHyperDriveStorage}`)
            await fs.rm(simulatedLocalHyperDriveStorage, { recursive: true });

            console.log("Global mocha hook: shutting down simulated remote server")
            //wait remoteServer.close()
            remoteServer.close() //Should await, but takes >10s. Just assume it will close eventually

            console.log(`Global mocha hook: removing the hyperspace files from ${simulatedRemoteHyperDriveStorage}`)
            await fs.rm(simulatedRemoteHyperDriveStorage, { recursive: true });

            console.log("Test will hang until servers are shut down, but feel free to end already")
        }
    };
};
