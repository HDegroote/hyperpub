const chai = require("chai");
const expect = chai.expect;
const schemas = require("../src/schemas/schemas")
const Ajv2020 = require("ajv/dist/2020")
const ajv = new Ajv2020()



describe("Test validateScehma", function() {
    let jsonObj
    let schema
    let validationFunction
    const key = "what"
    beforeEach(function () {
        jsonObj = {[key]: "that" }
        schema = {
            "properties": {
              [key]: {
                "type": "string",
              }}
        }
        validationFunction = ajv.compile(schema)

    });

      
    it("Check validateSchema standard exec", function() {    
      isValid = schemas.validateSchema(jsonObj, validationFunction)
      expect(isValid).to.be.true 
    });

    it("Check validateSchema standard exec upon failure", function() {            
        jsonObj[key] = 1
        expect(() => schemas.validateSchema(jsonObj, validationFunction)).to.throw() 
      });

    /*it("Check validateSchema standard exec with option to return False upon failure", function() {            
    jsonObj[key] = 1
    isValid = schemas.validateSchema(jsonObj, validationFunction, false)
    expect(isValid).to.be.false
    });
*/
  
});
