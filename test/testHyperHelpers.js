const helpers = require("../src/hyperHelpers")
//const simulator = require('hyperspace/simulator')
const chai = require("chai");
const expect = chai.expect;
const nodeAssert = require('assert/strict');
const workApi =require("../src/workApi")
const {Work} = require("../src/schemas/workSchema")
/*
describe("Test test", function(){
    it("test",  function() {
        console.log("ok")
        expect(true).to.be.true 
        //DEVNOTE: writable is False if a hypercore is requested which does not exist in the corestore
    });
});
*/

describe("Test hyperHelper methods", async function(){
    console.log("running tests")
    let client;
    const hash_key = "a".repeat(64)

    beforeEach(function () {  
        client = this.localClient
    });
    

    /*after(async function(){
        console.log("Cleaning up hyperspace simulator")
        await cleanup()
    });

    it("test",  function() {
        console.log("ok")
        expect(true).to.be.true 
        //DEVNOTE: writable is False if a hypercore is requested which does not exist in the corestore
    });*/



    describe("Test low-level hyper help methods", async function() {
    
    
        it("Check add_hyperdrive_to_corestore standard execution", async function() {
            const drive = await helpers.addHyperdriveToClient(client)            
            const drive_in_corestore = client.corestore().get(drive.key)
            await client.replicate(drive_in_corestore)            
            expect(drive_in_corestore.writable).to.be.true 
            //DEVNOTE: writable is False if a hypercore is requested which does not exist in the corestore
        });
    
        it("Check get_hyperdrive_from_corestore standard execution", async function() {
            const drive = await helpers.addHyperdriveToClient(client)
            const key = drive.key.toString("hex")        
            const read_drive = await helpers.getHyperdriveFromClient(client, key)
            expect(read_drive.writable).to.be.true //DEVNOTE: Ad hoc
        });
    
    })


    describe("Test json-level interactions with hyper help methods", async function() {
        let json_obj
        let drive
        const loc_in_drive = "/test.json"

        beforeEach(async function () {
            json_obj = {"title": "The title"}
            drive = await helpers.addHyperdriveToClient(client)
        });

        it("Check write_json_to_drive standard execution", async function() {
            await helpers.writeJsonToDrive(json_obj, drive, loc_in_drive)
            file = await drive.promises.readFile(loc_in_drive, 'utf8')
            recovered_json = JSON.parse(file.toString())
            expect(recovered_json).to.deep.equal(json_obj)
        });

        it("Check write_json_to_drive throws error when not passing json object", async function() {
            //DEVNOTE: could find no straightforward chai support, so using base assert library. See e.g. https://github.com/chaijs/chai/issues/415
            await nodeAssert.rejects(helpers.writeJsonToDrive("I am not json", drive, loc_in_drive))
        });

        it("Check read_json_from_drive standard execution", async function() {
            await helpers.writeJsonToDrive(json_obj, drive, loc_in_drive)
            recovered_json = await helpers.read_json_from_drive(drive, loc_in_drive)
            expect(recovered_json).to.deep.equal(json_obj)
        });

        it("Check read_json_from_drive invalid json throws error", async function() {
            await drive.promises.writeFile(loc_in_drive, "not json")
            await nodeAssert.rejects(helpers.read_json_from_drive(drive, loc_in_drive))
        });

        it("Check read_work_as_json standard execution", async function() {
            key = await workApi.create_work(json_obj, client)
            read_work = await helpers.read_work_as_json(key, client)
    
            gold = {
                [Work.HASH_KEY]: key, 
                [Work.TITLE_KEY]: json_obj.title
            }
            expect(read_work).to.deep.equal(gold)
        });
    
    

    });
      


});

