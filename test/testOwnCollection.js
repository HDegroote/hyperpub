const hyperHelpers = require("../src/hyperHelpers")
const chai = require("chai");
const expect = chai.expect;
const nodeAssert = require('assert/strict');
const {Work} = require("../src/schemas/workSchema")
const collectionSchema = require("../src/schemas/collectionSchema")
const ownCollectionHelpers = require("../src/ownCollectionHelpers")
const workApi = require("../src/workApi")

function createWork(title){
    return {[Work.TITLE_KEY]: title}
}


describe("Test validateCollectionOfOwnWork", async function(){
    let work, workHash
    let remoteWork, remoteWorkHash
    let client
    let remoteClient
    let ownCollectionOfClient
    //const dummyHash = "a".repeat(64)

    
    beforeEach(async function () {
        client = this.localClient
        remoteClient = this.remoteClient
        work = createWork("Work 1")
        workHash = await workApi.create_work(work, client)
        ownCollectionOfClient = collectionSchema.createCollectionFromTitleAndWorkHashes("Client collection", [workHash])
        remoteWork = createWork("Work 2")
        remoteWorkHash = await workApi.create_work(remoteWork, remoteClient)

    });
    
    
    it("Check validateCollectionOfOwnWork standard exec", async function() {
        const isValid = await ownCollectionHelpers.validateCollectionOfOwnWork(ownCollectionOfClient, client)
        expect(isValid).to.be.true
    });
        
    it("Check validateCollectionOfOwnWork throws error when it contains a not-own work", async function() {
        ownCollectionOfClient.works.push(remoteWorkHash)
        //console.log(ownCollectionOfClient)
        await nodeAssert.rejects(
            ownCollectionHelpers.validateCollectionOfOwnWork(ownCollectionOfClient, client), 
            ownCollectionHelpers.CollectionContainsUnwritableWorksError)
    });

    it("Check validateCollectionOfOwnWork standard exec", async function() {
        const ownCollection = await ownCollectionHelpers.createCollectionOfOwnWork(
            ownCollectionOfClient.title,
            ownCollectionOfClient.works, client)
        expect(ownCollection).to.deep.equal(ownCollectionOfClient)
    });

    it("Check validateCollectionOfOwnWork throws error when it contains a not-own work", async function() {
        ownCollectionOfClient.works.push(remoteWorkHash)
        await nodeAssert.rejects(
            ownCollectionHelpers.createCollectionOfOwnWork(
                ownCollectionOfClient.title,
                ownCollectionOfClient.works, client), 
            ownCollectionHelpers.CollectionContainsUnwritableWorksError)
    });
});
