# Hyperpub

Decentralised self-publishing, using the [hypercore protocol](https://hypercore-protocol.org/).

This project is still under development, and the current functionality is minimal. It currently allows publishing poetry, as well as collections of it. A simple console application has been implemented, allowing the transformation of .txt files with poems into a hypercore-based website.

This project is based on [ProPoPub](https://gitlab.com/HDegroote/propopub), an earlier attempt for a similar application developed in Python, but without direct integration with the Hypercore protocol.
However, working directly with the Hypercore protocol provides a number of interesting possibilities. 

## Usage

### Requirement: running hyperspace daemon
To use this package, you should have the hyperspace daemon (or equivalent) running before starting the application. 
The easiest way to do so is by installing the hyp client:

    npm install -g @hyperspace/cli

And to start the daemon:

    hyp daemon start


To be able to see the generated webpages, you will also need a browser which can handle the hypercore protocol.

The easiest option is probably [Beaker Browser](https://beakerbrowser.com/)


### Using the package

This package was developed in node v16

Install dependencies:
        
    npm install

Run the simple console application:
    
    node app.js

To create the website with the example poems, run `app.js` from the project directory, choose option `fromDir`, then enter `Poems of the World`

You will be able to see the generated collection of poems by entering the hypercore hash of the collection into a browser with hypercore support, such as Beaker Browser.


## Input format

Ideally, this application would have a proper front-end, but as for now minimal functionality is provided with a simple console application. 

The input format for a poem is a .txt file with as first line the title, and where verses are separated by a blank line. 

Tabs before the start of a line are rendered as indents: each tab is one level of indent (make sure you use the actual tab character). 
See the example of Fêtes de la faim.

If your poem contains chapters, these should be seperated from each other by two blank lines, and each should start with a title for the chapter (which is again followed by one blank line).
See the example of Preludes.

The examples can be found in the folder 'Poems of the World'. 

The input format for a collection is a .txt file with as first line the title, and where the works it contains (which can be any type of work, including other collections), are identified by their hypercore hash: one per line. For example:

```
An Example Collection

e869fd37a34c774c366da21f67350e2776524ad82dcb4518c746e1dcd340ca90
1ed57285fc4e69b4f64dafa852ba6227953be2e5029ff79c79a96bd6d8a7400a
428af35d12a924e91cfe667820b1d610e58771688ed2cb051013ec1b6a6ae177
```

(Note that the hyperlink hashes must refer to actual hyperdrives of the correct format, created by this application, and that they must be reachable at the time of creation. If not, the collection will not be created and an error will be thrown.)

## Brief description of the internal design

Each work (poem or collection) is described by a json file with semantic information (such as identifying the lines and verses of a poem). 

This JSON file is then transformed into an HTML page, and linked with a pre-defined CSS file.
These files are then stored on a hyperdrive, one for each work. A collection links to works based on their hypercore link. If you make a collection with works of someone else, it is recomended to help seed their works to ensure their availability whenever your collection is available. 

## Dev

Run the tests with

    mocha test

Note that the test setup currently connects with the actual Hyperswarm, which is not ideal (it takes a while to disconnect from it). In the future, the test setup should be impoved by using a local network. 

## Future work

- Expose Hypercore's versioning
- Allow deep links to other works (e.g. embedding a part of a poem into another poem)
- Support prose
- Support transforming a collection into a standard (non-hypercore) static website, hostable through http(s)
- Support Markdown and/or inline HTML for the input of poems, rather than just plain .txt
- Include a proper front-end application
- Consider developing in function of [atek cloud](https://atek.cloud/)
